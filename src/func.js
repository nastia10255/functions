const getSum = (str1, str2) => {
  let arr = "";
  if (typeof(str1) !== 'string' && typeof(str2) !== 'string') return false;
  if (isNaN(str1) || isNaN(str2)) return false;  
  if(str1.length >= str2.length)
  { 
    for (let i =0; i < str1.length; i ++)
    {
      if (str2[i]) arr += (Number(str2[i]) + Number(str1[i])).toString();
      else arr += str1[i];
    }
    return arr;
  }
  for (let i =0; i < str2.length; i ++)
  {
    if (str1[i]) arr += (Number(str2[i]) + Number(str1[i])).toString();
    else arr += str2[i];
  }
  return arr; 
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let post_count = listOfPosts.filter(obj => {
    return obj.author == authorName;
  }).length;  
  let c = 0;
  for(let list of listOfPosts)
  { let temp = list.comments;
    if (typeof(temp) !== 'undefined')
    { c +=  temp.filter(x => 
      {
        return x.author == authorName;
      }).length;   
    }
  }
  return "Post:" + post_count + ",comments:" + c;
}

const tickets=(peopleInLine) => {
    let total25bill = 0;
    let total50bill = 0;
    let total100bill = 0;
    for (let people of peopleInLine) 
    {
      if (people === 25) total25bill += 1; 
      else if (people === 50) 
      {
          if (total25bill >= 1) {
              total25bill -= 1;
              total50bill += 1;
          } else {
              return "NO";
          }
      } 
      else if (people === 100) 
      {
          if ((total50bill >= 1) && (total25bill >= 1))
          {
              total25bill -= 1; 
              total50bill -= 1; 
              total100bill += 1;
          } else if (total25bill >= 3){
              total25bill -= 3; 
              total100bill += 1;
          } else return "NO";
      }
    }
    return "YES";
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
